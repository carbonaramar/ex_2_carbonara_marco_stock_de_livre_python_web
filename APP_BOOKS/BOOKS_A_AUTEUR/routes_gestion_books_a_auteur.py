# routes_gestion_books_a_auteur.py
# OM 2020.04.16 Gestions des "routes" FLASK pour la table intermédiaire qui associe les books et l'auteur.
from flask import render_template
from APP_BOOKS import obj_mon_application


# ---------------------------------------------------------------------------------------------------
# OM 2020.04.07 Définition d'une "route" /books_a_auteur_afficher
# cela va permettre de programmer les actions avant d'interagir
# avec le navigateur par la méthode "render_template"
# Pour tester http://127.0.0.1:1234/books_a_auteur_afficher
# ---------------------------------------------------------------------------------------------------
@obj_mon_application.route("/books_a_auteur_afficher", methods=['GET', 'POST'])
def books_a_auteur_afficher():
    # # OM 2020.04.09 Pour savoir si les données d'un formulaire sont un affichage
    # # ou un envoi de donnée par des champs du formulaire HTML.
    # if request.method == "GET":
    #     try:
    #         # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
    #         obj_actions_books_a_auteur = GestionBooksAuteur()
    #         # Récupére les données grâce à une requête MySql définie dans la classe GestionAuteur()
    #         # Fichier data_gestion_auteur.py
    #         data_books_a_auteur = obj_actions_books_a_auteur.auteur_afficher_data()
    #         # DEBUG bon marché : Pour afficher un message dans la console.
    #
    #         # OM 2020.04.09 La ligns ci-après permet de donner un sentiment rassurant aux utilisateurs.
    #         flash("Données auteur de books affichées !!", "Success")
    #     except Exception as erreur:
    #         print(f"RGFG Erreur générale.")
    #         # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)" fichier "run_mon_app.py"
    #         # Ainsi on peut avoir un message d'erreur personnalisé.
    #         # flash(f"RGG Exception {erreur}")
    #         raise Exception(f"RGFG Erreur générale. {erreur}")

    # OM 2020.04.07 Envoie la page "HTML" au serveur.
    return render_template("books_a_auteur/books_a_auteur_afficher.html")

