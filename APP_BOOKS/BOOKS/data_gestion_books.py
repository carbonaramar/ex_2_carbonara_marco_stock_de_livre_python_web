# data_gestion_books.py
# OM 2698.03.21 Permet de gérer (CRUD) les données de la table t_books


from flask import flash

from APP_BOOKS.DATABASE import connect_db_context_manager
from APP_BOOKS import obj_mon_application
from APP_BOOKS.DATABASE.connect_db_context_manager import MaBaseDeDonnee
from APP_BOOKS.DATABASE.erreurs import *



class GestionBooks():
    def __init__(self):
        try:
            print("dans le try de gestions books")
            # OM 2020.04.11 La connexion à la base de données est-elle active ?
            # Renvoie une erreur si la connexion est perdue.
            MaBaseDeDonnee().connexion_bd.ping(False)
        except Exception as erreur:
            flash("Dans Gestion books ...terrible erreur, il faut connecter une base de donnée", "Danger")
            # DEBUG bon marché : Pour afficher un message dans la console.
            print(f"Exception grave Classe constructeur GestionAuteur {erreur.args[0]}")
            raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

        print("Classe constructeur GestionBooks ")


    def books_afficher_data(self):
        try:
            # OM 2020.04.07 C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
            # la commande MySql classique est "SELECT * FROM t_books"
            # Pour "lever"(raise) une erreur s'il y a des erreurs sur les noms d'attributs dans la table
            # donc, je précise les champs à afficher
            strsql_books_afficher = """SELECT id_Books, Isbn_Books, Titre_Books, Country_Books,
                                       Date_Books, Publieur_Books FROM t_books"""
            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                # Envoi de la commande MySql
                mc_afficher.execute(strsql_books_afficher)
                # Récupère les données de la requête.
                data_books = mc_afficher.fetchall()
                # Affichage dans la console
                print("data_books ", data_books, " Type : ", type(data_books))
                # Retourne les données du "SELECT"
                return data_books
        except pymysql.Error as erreur:
            print(f"DGF gad pymysql errror {erreur.args[0]} {erreur.args[1]}")
            raise  MaBdErreurPyMySl(f"DGG fad pymysql errror {msg_erreurs['ErreurPyMySql']['message']} {erreur.args[0]} {erreur.args[1]}")
        except Exception as erreur:
            print(f"DGF gad Exception {erreur.args}")
            raise MaBdErreurConnexion(f"DGG fad Exception {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args}")
        except pymysql.err.IntegrityError as erreur:
            # OM 2020.04.09 On dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            # raise MaBdErreurDoublon(f"{msg_erreurs['ErreurDoublonValue']['message']} et son status {msg_erreurs['ErreurDoublonValue']['status']}")
            raise MaBdErreurConnexion(f"DGF fad pei {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[1]}")



    # def add_books(self, Isbn_Books, Titre_Books, Country_Books):
    def add_books(self, valeurs_insertion_dictionnaire):
        try:
            # # Définitions d'un dictionnaire pour passer les valeurs en paramètres de façon un "peu" sécurisée dans la BD
            # valeurs_insertion_dictionnaire = {'value_Isbn_Books': valeur_ins_1, 'value_Titre_Books': valeur_ins_2,
            #                                   'Country_Books': valeur_ins_3}
            # Rssure la personne qui dévelloppe que les valeurs à insérer sont bien à disposition.
            print(valeurs_insertion_dictionnaire)
            str_sql_insert = "INSERT INTO t_books (id_Books, Isbn_Books, Titre_Books, Country_Books, " \
                             "Date_Books, Publieur_Books) VALUES (NULL, %(value_Isbn_Books)s, %(value_Titre_Books)s, " \
                             "%(value_Country_Books)s, %(value_Date_Books)s, %(value_Publieur_Books)s)"
            with MaBaseDeDonnee() as ma_bd_curseur:
                # OM Méthode "execute" définie simplement pour raccourcir la ligne de code
                # ligne de code normale : ma_bd_moi.connexion_bd.cursor(str_sql_insert, valeurs_insertion_dictionnaire)
                ma_bd_curseur.execute(str_sql_insert, valeurs_insertion_dictionnaire)

        except Exception as erreur:
            # OM 2020.04.09 DIFFERENTS MOYENS D'INFORMER EN CAS D'ERREURS.
            # Message dans la console en cas d'échec du bon déroulement des commandes ci-dessus.
            print("Data Gestions Books ERREUR: {0}".format(erreur))
            print(f"Print console ... Data Gestions Books, numéro de l'erreur : {erreur}")
            # Petits messages "flash", échange entre Python et Jinja dans une page en HTML
            flash(f"Flash ... Data Gestions Books, numéro de l'erreur : {erreur}")
            # raise, permet de "lever" une exception et de personnaliser la page d'erreur
            # voir fichier "run_mon_app.py"

            print("erreur args.. ",erreur.args)
            code, msg = erreur.args
            print(" codes d'erreurs ---> ", error_codes.get(code, msg))
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)" fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise Exception(f"Raise exception... Data Gestions Books {erreur}")