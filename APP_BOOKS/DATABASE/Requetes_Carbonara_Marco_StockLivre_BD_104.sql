/*
	Toutes les colonnes
*/
SELECT * FROM t_books_a_genres AS T1
INNER JOIN t_genres AS T2 ON T2.Id_Genres = T1.FK_Genres
INNER JOIN t_books AS T3 ON T3.id_Books = T1.FK_Books

/*
	Seulement certaines colonnes
*/
SELECT id_Auteur, Nom_Auteur , id_Books, Titre_Books FROM t_books_a_auteur AS T1
INNER JOIN t_books AS T2 ON T2.id_Books = T1.FK_Books
INNER JOIN t_auteur AS T3 ON T3.id_Auteur = T1.FK_Auteur

/* 	
	Permet d'aficher toutes les lignes de la table de droite (t_format) (qui est écrite en sql à droite de t_genres_films)
	y compris les lignes qui ne sont pas attribuées à des books.
*/
SELECT id_Format, Nom_Format , id_Books, Titre_Books FROM t_books_a_format AS T1
INNER JOIN t_books AS T2 ON T2.id_Books = T1.FK_Books
RIGHT JOIN t_format AS T3 ON T3.id_Format = T1.FK_Format

/* 	
	Permet d'aficher toutes les lignes de la table de droite (t_illustrateur) (qui est écrite en sql à droite de t_genres_films)
	y compris les lignes qui ne sont pas attribuées à des Books.
*/
SELECT id_Illustrateur, Nom_Illustrateur , id_Books, Titre_Books  FROM t_books_a_illustrateur AS T1
RIGHT JOIN t_books AS T2 ON T2.id_Books = T1.FK_Books
LEFT JOIN t_illustrateur AS T3 ON T3.id_Illustrateur = T1.FK_Illustrateur


/*
	Affiche TOUS les Books qui n'ont pas de langue attribués
*/
SELECT Id_Langue, Dispo_Langue , id_Books, Titre_Books  FROM t_books_a_langue AS T1
RIGHT JOIN t_books AS T2 ON T2.id_Books = T1.FK_Books
LEFT JOIN t_langue AS T3 ON T3.Id_Langue = T1.FK_Langue


/*
	Affiche SEULEMENT les Books qui n'ont pas de Page attribués
*/

SELECT Id_Page, No_Page , id_Books, Titre_Books  FROM t_books_a_page AS T1
RIGHT JOIN t_books AS T2 ON T2.id_Books = T1.FK_Books
LEFT JOIN t_page AS T3 ON T3.Id_Page = T1.FK_Page
WHERE T1.FK_Page IS NULL